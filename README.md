# Library for hackaton

## install

### Для того чтобы установить пакет по ssh:

poetry add git+ssh://git@gitlab.com:leaders_digital_transformations/hack_lib.git

### Для того чтобы установить пакет по http (не желательно):

poetry add git+https://gitlab.com/leaders_digital_transformations/hack_lib

## Usage

```python
from hack_lib import IMLModel

from hack_lib import PhotoMetadata 
from hack_lib import QueryProcessPhotoDto 
from hack_lib import MLResult 
from hack_lib import ResultProcessPhotoDto
```
