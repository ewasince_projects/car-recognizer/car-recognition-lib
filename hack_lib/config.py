import os

from pydantic import BaseModel, Field


class Config(BaseModel):
    """
    Основной класс конфига, выполняющий проверку всех полей
    """

    def __new__(cls):
        if hasattr(cls, "_instance"):
            return cls._instance

        # создание конфига
        config_dict = {}

        for param in cls.model_fields:
            param: str
            var = os.environ.get(param.upper())
            if var is not None:
                config_dict[param] = var

        cls._instance = super(Config, cls).__new__(cls)
        super(Config, cls._instance).__init__(**config_dict)

        return cls._instance

    # noinspection PyMissingConstructor
    def __init__(self, *args, **kwargs):
        pass

    ####################

    # secrets

    # public config
    log_level_console: str = Field(default="INFO")
    log_level_file: str = Field(default="DEBUG")
    log_file: str = Field(default="./logs/service.log")
    keep_log_files: str = Field(default="14")

    broker_username: str = Field(default="hack_user")
    broker_password: str = Field(default="hack_user")
    broker_host: str = Field(default="localhost")
    broker_port: int = Field(default=5672)
    broker_channel_tasks: str = Field(default="tasks")
    broker_channel_completed: str = Field(default="completed")
