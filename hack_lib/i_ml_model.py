from abc import ABC, abstractmethod
from typing import Generator, Any

from PIL.Image import Image

from hack_lib.result_process_photo_dto import MLResult


class IMLModel(ABC):
    """Интерфейс слейв ноды, который будет реализовывать модель"""

    def __init__(self):
        self.last_updated = 0
        return

    @abstractmethod
    def get_weights(self) -> str:
        """
        Возвращает веса модели

        :return: путь до файла с весами
        """
        raise NotImplementedError

    @abstractmethod
    def update_weights(self, weights_filename: str) -> bool:
        """
        Обновляет веса у модели

        :param weights: путь до файла с весами, которые нужно установить
        """
        raise NotImplementedError

    @abstractmethod
    def additional_train(self, train_data_path: str) -> bool:
        """
        Обновляет веса у модели

        Папки с данными включают в себя папки images (0001.png и т.д.) и labels (0001.txt и т.д.)

        :param train_data_path: папка с тренировочными данными *train*
        """
        raise NotImplementedError

    @abstractmethod
    def retrain(
        self,
        train_data_path: str,
        test_data_path: str,
        controll_data_path: str,
    ) -> bool:
        """
        Обновляет веса у модели

        Папки с данными включают в себя папки images (0001.png и т.д.) и labels (0001.txt и т.д.)

        :param train_data_path: папка с тренировочными данными *train*
        :param test_data_path: папка с тестовыми данными *test*
        :param controll_data_path: папка с контрольными данными *valid*
        """
        raise NotImplementedError

    @abstractmethod
    def process_video(self, source: Any) -> Generator[MLResult, None, None]:
        """
        Обрабатывает видео

        :return: Генератор результатов мл-модели, см. MLResult
        """
        raise NotImplementedError

    @abstractmethod
    def process_image(self, photo: Image) -> MLResult:
        """
        Обрабатывает картинку (кадр)

        :return: Результат мл-модели, см. MLResult
        """
        raise NotImplementedError
