import pickle
from typing import Any, AsyncIterator

import aio_pika
from aio_pika import IncomingMessage
from aio_pika.abc import AbstractChannel, AbstractConnection, AbstractQueue

from hack_lib import QueryProcessPhotoDto, ResultProcessPhotoDto
from hack_lib.config import Config
from hack_lib.utils import make_url


class Rabbit:
    """
    Rabbit wrapper with singletone connection

    WARNING!! one instance per one thread!
    """

    def __init__(self, config: Config):
        self.config = config
        self._connection: AbstractConnection = None
        return

    async def _establish_connection(self) -> AbstractConnection:
        if self._connection:
            return self._connection

        rmq_url = make_url(
            host=self.config.broker_host,
            port=self.config.broker_port,
            login=self.config.broker_username,
            password=self.config.broker_password,
        )

        self._connection = await aio_pika.connect(rmq_url)

        return self._connection

    async def get_channel(self) -> AbstractChannel:
        connection = await self._establish_connection()
        return await connection.channel()

    async def send_task(
        self,
        channel,
        routing_key: str,
        data: QueryProcessPhotoDto | ResultProcessPhotoDto,
    ) -> None:
        # print(f"sent task {data.metadata.photo_UUID}")
        data_bytes = pickle.dumps(data)
        await channel.default_exchange.publish(
            aio_pika.Message(body=data_bytes),
            routing_key=routing_key,
        )

    async def consume_result(self, queue) -> AsyncIterator[Any]:
        async with queue.iterator() as queue_iter:
            async for message in queue_iter:
                message: IncomingMessage

                async with message.process():
                    data = pickle.loads(message.body)
                    yield data

    async def declare_queue(
        self,
        channel,
        routing_key: str,
        arguments=None,
        auto_delete=False,
        ttl=1000 * 3,
        dlx=False,
    ) -> AbstractQueue:
        second = 1000
        seconds = second * 3
        arguments = arguments or {}
        if ttl:
            arguments["x-message-ttl"] = ttl
        if dlx:
            arguments["x-dead-letter-exchange"] = "dlx_incoming"

        try:
            queue = await channel.declare_queue(
                routing_key, arguments=arguments, auto_delete=auto_delete
            )
        except Exception as e:
            raise e
        return queue
