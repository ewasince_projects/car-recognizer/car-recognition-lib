from dataclasses import dataclass

from hack_lib.query_process_photo_dto import PhotoMetadata

from pydantic import BaseModel


class Box(BaseModel):
    coords: tuple[float, float, float, float]
    cls_: str


class MLResult(BaseModel):
    result: bool
    boxes: list[Box]


@dataclass
class ResultProcessPhotoDto:
    result: MLResult
    metadata: PhotoMetadata
