from enum import Enum


class MlTopics(Enum):
    weapon = 'weapon'
    poses = 'poses'
    other = 'other'
    test = 'test'

class OtherTopics(Enum):
    ml_result = 'ml_result'
    picture_result = 'picture_result'
